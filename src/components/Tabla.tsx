import { useState, useMemo } from "react";
import { Usuario } from "../interfaces/Usuarios";
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from "@tanstack/react-table";

interface props {
  data: Usuario[];
  handleChange: (status: boolean, name: string) => void;
}

const Tabla = ({ data, handleChange }: props) => {
  const [rowSelection, setRowSelection] = useState({});

  const columns = useMemo<ColumnDef<Usuario>[]>(
    () => [
      {
        id: "select",
        cell: ({ row }) => {
          return (
            <input
              type="checkbox"
              onChange={(e) =>
                handleChange(e.target.checked, row.original.name)
              }
              checked={row.original.status}
            />
          );
        },
      },
      {
        id: "name",
        accessorKey: "name",
        cell: (info) => info.getValue(),
      },
    ],
    []
  );

  const table = useReactTable({
    data: data,
    columns,
    state: {
      rowSelection,
    },
    enableRowSelection: true,
    onRowSelectionChange: setRowSelection,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
  });

  return (
    <table className="table">
      <tbody>
        {table.getRowModel().rows.map((row) => (
          <tr key={row.id}>
            {row.getVisibleCells().map((cell) => (
              <td key={cell.id}>
                {flexRender(cell.column.columnDef.cell, cell.getContext())}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Tabla;

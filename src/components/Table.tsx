import { Usuario } from "../interfaces/Usuarios";

interface props {
  data: Usuario[];
  handleChange: (status: boolean, name: string) => void;
}

const Table = ({ data, handleChange }: props) => {
  return (
    <table className="table">
      <tbody>
        {data.map((d) => (
          <tr>
            <td>{d.name}</td>
            <td>
              <input
                type="checkbox"
                onChange={(e) => handleChange(e.target.checked, d.name)}
                checked={d.status}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;

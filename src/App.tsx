import Table from "./components/Table";
import { useState } from "react";
import { Usuario } from "./interfaces/Usuarios";

const initialState: Usuario[] = [
  {
    name: "Francisco",
    area: "Frontend",
    status: false,
  },
  {
    name: "Daneli",
    area: "Frontend",
    status: false,
  },
  {
    name: "Ivan",
    area: "Backend",
    status: false,
  },
  {
    name: "Martin",
    area: "Backend",
    status: false,
  },
];

const App = () => {
  const [usuarios, setUsuarios] = useState<Usuario[]>(initialState);

  const handleChange = (status: boolean, name: string) => {
    const newArray = usuarios.map((u) => {
      if (u.name === name) u.status = status;
      return u;
    });

    setUsuarios(newArray);
  };

  const changeArea = (area: string) => {
    const newArray = usuarios.map((u) => {
      if (u.status) u.area = area;
      return { ...u, status: false };
    });

    console.log(newArray);
    setUsuarios(newArray);
  };

  return (
    <div className="container">
      <h1 className="mb">Area de usuarios</h1>

      <div className="user_container">
        <div>
          <h2 className="mb">Frontend</h2>
          <Table
            data={usuarios.filter((u) => u.area === "Frontend")}
            handleChange={handleChange}
          />
        </div>

        <div className="button_container">
          <button className="button" onClick={() => changeArea("Backend")}>
            {">"}
          </button>
          <button className="button" onClick={() => changeArea("Frontend")}>
            {"<"}
          </button>
        </div>

        <div>
          <h2 className="mb">Backend</h2>
          <Table
            data={usuarios.filter((u) => u.area === "Backend")}
            handleChange={handleChange}
          />
        </div>
      </div>
    </div>
  );
};

export default App;

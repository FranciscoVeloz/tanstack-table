export interface Usuario {
  name: string;
  area: string;
  status: boolean;
}
